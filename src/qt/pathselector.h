#ifndef __PATH_SELECTOR__
#define __PATH_SELECTOR__

#include <QtGui>

class PathSelector : public QToolButton
{
    Q_OBJECT

public:
    PathSelector(QWidget* parent = 0);

    void setModel(QFileSystemModel* model);

private:
    QFileSystemModel* m_model;

private slots:
    void openFileDialog();

    void setPathLabel(const QString& path);
};

#endif
