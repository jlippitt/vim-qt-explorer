#ifndef __FILE_TREE__
#define __FILE_TREE__

#include <QtGui>
#include <fileexplorer.h>

class FileTree : public QTreeView
{
    Q_OBJECT

public:
    FileTree(QWidget* parent = 0);

    void setModel(QAbstractItemModel* model);

protected:
    void dragEnterEvent(QDragEnterEvent* event);

    void dragMoveEvent(QDragMoveEvent* event);

    void dropEvent(QDropEvent* event);

    void mouseDoubleClickEvent(QMouseEvent* event);

    void mouseMoveEvent(QMouseEvent* event);

    void mousePressEvent(QMouseEvent* event);

signals:
    void fileCreated(const QString& path);

    void fileSelected(const QString& path, FileExplorer::FileOpenTarget target);

private:
    QPoint m_dragStartPosition;

    QFileSystemModel* fileModel();

    const QFileSystemModel* fileModel() const;

    QDir getDirectory(const QModelIndex& index) const;

    void recursiveDelete(const QFileInfo& info);

private slots:
    void setRootPath(const QString& path);
};

#endif
