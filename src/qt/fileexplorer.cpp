#include <fileexplorer.moc>
#include <filetree.h>
#include <pathselector.h>

FileExplorer::FileExplorer(QWidget* parent)
    : QWidget(parent),
      m_model(new QFileSystemModel(this))
{
    PathSelector* pathSelector = new PathSelector(this);
    pathSelector->setModel(m_model);

    FileTree* fileTree = new FileTree(this);
    fileTree->setModel(m_model);

    connect(
        fileTree,
        SIGNAL(fileCreated(const QString&)),
        this,
        SLOT(createFile(const QString&))
    );

    connect(
        fileTree,
        SIGNAL(fileSelected(const QString&, FileExplorer::FileOpenTarget)),
        this,
        SLOT(selectFile(const QString&, FileExplorer::FileOpenTarget))
    );

    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->addWidget(pathSelector);
    layout->addWidget(fileTree);

    m_model->setRootPath(QDir::currentPath());
}

void FileExplorer::createFile(const QString& path)
{
    emit fileCreated(path);
}

void FileExplorer::selectFile(const QString& path, FileExplorer::FileOpenTarget target)
{
    emit fileSelected(path, target);
}

