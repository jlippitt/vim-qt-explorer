#include <filetree.moc>

FileTree::FileTree(QWidget* parent)
    : QTreeView          (parent),
      m_dragStartPosition()
{
    setHeaderHidden(true);
    setAcceptDrops(true);
    
    resize(240, 0);
}

void FileTree::setModel(QAbstractItemModel* model)
{
    QTreeView::setModel(model);

    for (int i = 1; i < fileModel()->columnCount(); ++i)
    {
        hideColumn(i);
    }

    connect(
        fileModel(),
        SIGNAL(rootPathChanged(const QString&)),
        this,
        SLOT(setRootPath(const QString&))
    );
}

void FileTree::dragEnterEvent(QDragEnterEvent* event)
{
    if (event->mimeData()->hasUrls())
    {
        event->acceptProposedAction();
    }
}

void FileTree::dragMoveEvent(QDragMoveEvent* event)
{
    QWidget::dragMoveEvent(event);
}

void FileTree::dropEvent(QDropEvent* event)
{
    const QMimeData* mimeData = event->mimeData();

    foreach (QUrl url, mimeData->urls())
    {
        if (!url.isLocalFile())
        {
            continue;
        }

        // Source file path
        QFileInfo source(url.toLocalFile());

        // Destination directory
        QDir destination = getDirectory(indexAt(event->pos()));

        destination.rename(source.filePath(), destination.filePath(source.fileName()));
    }

    event->acceptProposedAction();
}

void FileTree::mouseDoubleClickEvent(QMouseEvent* event)
{
    const QModelIndex& index = indexAt(event->pos());
    
    if (!fileModel()->isDir(index))
    {
        emit fileSelected(fileModel()->filePath(index), FileExplorer::OpenInTab);
    }

    QTreeView::mouseDoubleClickEvent(event);
}

void FileTree::mouseMoveEvent(QMouseEvent* event)
{
    if (event->buttons() & Qt::LeftButton &&
        (event->pos() - m_dragStartPosition).manhattanLength() >= QApplication::startDragDistance())
    {
        QDrag* drag = new QDrag(this);

        // Store the local file we're dragging as a URL in the mime data
        QList<QUrl> urls;
        urls.append(QUrl::fromLocalFile(fileModel()->filePath(indexAt(event->pos()))));

        QMimeData* mimeData = new QMimeData();
        mimeData->setUrls(urls);

        drag->setMimeData(mimeData);
        drag->exec();
    }
}

void FileTree::mousePressEvent(QMouseEvent* event)
{
    QTreeView::mousePressEvent(event);

    if (event->button() == Qt::LeftButton)
    {
        m_dragStartPosition = event->pos();
    }
    else if (event->button() == Qt::RightButton)
    {
        const QModelIndex& index = indexAt(event->pos());

        QDir currentDirectory = getDirectory(index);

        QMenu contextMenu;

        QAction* newFileAction = contextMenu.addAction(tr("New File"));
        QAction* newFolderAction = contextMenu.addAction(tr("New Folder"));
        QAction* renameAction = contextMenu.addAction(tr("Rename..."));
        QAction* deleteFileAction = contextMenu.addAction(tr("Delete Selected File"));

        contextMenu.addSeparator();

        QAction* openInTabAction = contextMenu.addAction(tr("Open Selected File in Tab"));
        QAction* openInHsplitAction = contextMenu.addAction(tr("Open Selected File in Horizontal Split"));
        QAction* openInVsplitAction = contextMenu.addAction(tr("Open Selected File in Vertical Split"));
        QAction* changeDirectoryAction = contextMenu.addAction(tr("Change working directory to ") + currentDirectory.dirName());

        QAction* selectedAction = contextMenu.exec(event->globalPos());

        if (selectedAction == newFileAction)
        {
            QString name = QInputDialog::getText(
                this,
                tr("Create File"),
                tr("File name:")
            );

            if (!name.isNull() && !name.isEmpty())
            {
                emit fileCreated(currentDirectory.path() + "/" + name);

                // Expand parent folder (will do nothing if 'index' refers to a file)
                expand(index);
            }
        }
        else if (selectedAction == newFolderAction)
        {
            QString name = QInputDialog::getText(
                this,
                tr("Create Folder"),
                tr("Folder name:")
            );

            if (!name.isNull() && !name.isEmpty())
            {
                currentDirectory.mkdir(name);

                // Expand parent folder (will do nothing if 'index' refers to a file)
                expand(index);
            }
        }
        else if (selectedAction == renameAction)
        {
            QString oldName = fileModel()->fileName(index);

            QString newName = QInputDialog::getText(
                this,
                tr("Rename"),
                tr("New name:"),
                QLineEdit::Normal,
                oldName
            );

            if (!newName.isNull() && !newName.isEmpty())
            {
                fileModel()->fileInfo(index).dir().rename(oldName, newName);
            }
        }
        else if (selectedAction == deleteFileAction)
        {
            recursiveDelete(fileModel()->fileInfo(index));
        }
        else if (selectedAction == openInTabAction)
        {
            emit fileSelected(fileModel()->filePath(index), FileExplorer::OpenInTab);
        }
        else if (selectedAction == openInHsplitAction)
        {
            emit fileSelected(fileModel()->filePath(index), FileExplorer::OpenInHsplit);
        }
        else if (selectedAction == openInVsplitAction)
        {
            emit fileSelected(fileModel()->filePath(index), FileExplorer::OpenInVsplit);
        }
        else if (selectedAction == changeDirectoryAction)
        {
            fileModel()->setRootPath(currentDirectory.path());
        }
    }
}

QFileSystemModel* FileTree::fileModel()
{
    return dynamic_cast<QFileSystemModel*>(model());
}

const QFileSystemModel* FileTree::fileModel() const
{
    return dynamic_cast<const QFileSystemModel*>(model());
}

QDir FileTree::getDirectory(const QModelIndex& index) const
{
    if (fileModel()->fileInfo(index).isDir())
    {
        return QDir(fileModel()->filePath(index));
    }
    else
    {
        return fileModel()->fileInfo(index).dir();
    }
}

void FileTree::recursiveDelete(const QFileInfo& fileInfo)
{
    if (fileInfo.isDir())
    {
        QDir dir(fileInfo.canonicalFilePath());

        QDir::Filters filters =
            QDir::AllEntries |
            QDir::NoDotAndDotDot |
            QDir::Hidden |
            QDir::System;

        // Remove all children recursively
        foreach (const QFileInfo& child, dir.entryInfoList(filters))
        {
            recursiveDelete(child);
        }

        fileInfo.dir().rmdir(fileInfo.fileName());
    }
    else
    {
        fileInfo.dir().remove(fileInfo.fileName());
    }
}

void FileTree::setRootPath(const QString& path)
{
    setRootIndex(fileModel()->index(path));
}

