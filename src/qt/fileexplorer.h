#ifndef __FILE_EXPLORER__
#define __FILE_EXPLORER__

#include <QtGui>

class FileExplorer : public QWidget
{
    Q_OBJECT

public:
    enum FileOpenTarget
    {
        OpenInTab = 0,
        OpenInHsplit,
        OpenInVsplit
    };

    FileExplorer(QWidget* parent = 0);

signals:
    void fileCreated(const QString& path);

    void fileSelected(const QString& path, FileExplorer::FileOpenTarget target);

private:
    QFileSystemModel* m_model;

private slots:
    void createFile(const QString& path);

    void selectFile(const QString& path, FileExplorer::FileOpenTarget target);
};

#endif
