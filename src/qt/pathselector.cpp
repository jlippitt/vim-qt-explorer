#include <pathselector.moc>

PathSelector::PathSelector(QWidget* parent)
    : QToolButton(parent)
{
    setIcon(QIcon(":/icons/folder-open.png"));
    setAutoRaise(true);
    setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

    connect(
        this,
        SIGNAL(clicked()),
        this,
        SLOT(openFileDialog())
    );
}

void PathSelector::setModel(QFileSystemModel* model)
{
    m_model = model;

    connect(
        m_model,
        SIGNAL(rootPathChanged(const QString&)),
        this,
        SLOT(setPathLabel(const QString&))
    );
}

void PathSelector::openFileDialog()
{
    QString path = QFileDialog::getExistingDirectory(
        this,
        tr("Select root folder"),
        m_model->rootPath(),
        QFileDialog::ShowDirsOnly
    );

    if (!path.isNull())
    {
        m_model->setRootPath(path);
    }
}

void PathSelector::setPathLabel(const QString& path)
{
    setText(path);
}

