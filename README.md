# QVim File Explorer

This is a file browser fork of Vim-Qt (a.k.a. QVim) offering similar functionality to the [MacVim file browser fork](https://github.com/alloy/macvim/).

## Installation

For compilation and installation instructions, see the [Vim-Qt repository overview](https://bitbucket.org/equalsraf/vim-qt/wiki/Home).

